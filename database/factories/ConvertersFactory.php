<?php

namespace Database\Factories;

use App\Models\Converters;
use Illuminate\Database\Eloquent\Factories\Factory;

class ConvertersFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Converters::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

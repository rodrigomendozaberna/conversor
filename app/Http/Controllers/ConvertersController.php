<?php

namespace App\Http\Controllers;

use App\Models\Converters;
use Illuminate\Http\Request;

class ConvertersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $history = Converters::all();

        return view('unit.index', compact('history'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mb = $request->mb;
        Converters::create([
            'mb' => $mb,
            'kb' => $mb * 1024,
            'gb' => $mb / 1024,
            'tb' => $mb / 125000
        ]);

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Converters $converters
     * @return \Illuminate\Http\Response
     */
    public function show(Converters $converters)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Converters $converters
     * @return \Illuminate\Http\Response
     */
    public function edit(Converters $converters)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Converters $converters
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Converters $converters)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Converters $converters
     * @return \Illuminate\Http\Response
     */
    public function destroy(Converters $converters)
    {
        $converters = Converters::findOrFail($converters);

        $converters->delete();

        return redirect('/');
    }
}

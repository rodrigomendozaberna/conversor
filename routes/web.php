<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\ConvertersController::class, 'index']);
Route::post('unit', [\App\Http\Controllers\ConvertersController::class, 'store']);
Route::delete('unit/{id}', [\App\Http\Controllers\ConvertersController::class, 'destroy']);
